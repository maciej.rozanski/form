<?php

include 'config/Config.php';

$GLOBALS['DB'] = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

if ($GLOBALS['DB']->connect_error) {
    die("Błąd połączenia z bazą danych: " . $GLOBALS['DB']->connect_error);
}

