<?php

class Form {
    
    private $firstname;
    private $lastname;
    private $image;
    
    public function __construct($firstname, $lastname, $image) { 
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->image = array(
            'name' => $image['name'],
            'type' => $image['type'],
            'tmp_name' => $image['tmp_name']
        );
    }
    
    public function save(){
        
        $this->firstname = Form::secureInput($this->firstname);
        $this->lastname = Form::secureInput($this->lastname);
        
        if(empty($this->firstname)){
            echo json_encode(array('status'=>false, 'message'=>'[Imię] Pole jest puste'));
            exit;
        }
        
        if($this->image['type']!='image/jpeg' && $this->image['type']!='image/png') {
            echo json_encode(array('status'=>false, 'message'=>'[Plik] Dozwolony w formacie jpg lub png'));
            exit;
        }
        
        if(file_exists(TARGET_DIR . basename($this->image['name']))) {
            $name = pathinfo($this->image['name'], PATHINFO_FILENAME);
            $extension = pathinfo($this->image['name'], PATHINFO_EXTENSION);
            $this->image['name'] = $name.'_1.'.$extension;
        }
        $upload = move_uploaded_file($this->image['tmp_name'], TARGET_DIR . basename($this->image['name']));
        
        $query = sprintf('INSERT INTO form (firstname, lastname, image) VALUES("%s","%s","%s")',
                mysqli_real_escape_string($GLOBALS['DB'],$this->firstname),
                mysqli_real_escape_string($GLOBALS['DB'],$this->lastname),
                mysqli_real_escape_string($GLOBALS['DB'],$this->image['name'])
                );
        $insert = $GLOBALS['DB']->query($query);
        
        if(!$insert) {
            echo json_encode(array('status'=>false, 'message'=>'Błąd przy zapisie do bazy danych: ' . $GLOBALS['DB']->error));
            exit;
        }
        
        if($upload && $insert) {
            echo json_encode(array('status'=>true, 'message'=>'Dodano pomyślnie'));
            exit;
        }
    }
    
    public static function getRecords() {
        $query = 'SELECT firstname, lastname, image FROM form';
        return $GLOBALS['DB']->query($query);
    }
    
    private static function secureInput($text) {
        return preg_replace('/[^a-zA-Z0-9_ąęśćłóńżźĄĘŚĆŁÓŃŻŹ, @.:\/~-]/i','',$text);
    }
 
}