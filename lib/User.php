<?php

class User {
    
    public $username;
    public $password;
    
    public function __construct() { 
        $this->username = '';
        $this->password = '';
    }
    
    public static function getUserByUsername($username) {
        $username = User::secureInput($username);
        $user = new User();
        $query = sprintf('SELECT username, password FROM user WHERE username="%s"',
                mysqli_real_escape_string($GLOBALS['DB'],$username));
        $result = $GLOBALS['DB']->query($query);
        if($result->num_rows) {
            $row = $result->fetch_assoc();
            $user->username = $row['username'];
            $user->password = $row['password'];
        }
        return $user;
    }
    
    private static function secureInput($text) {
        return preg_replace('/[^a-zA-Z0-9_ąęśćłóńżźĄĘŚĆŁÓŃŻŹ, @.:\/~-]/i','',$text);
    }
}