<?php

include 'lib/DB_Connect.php';
include 'lib/User.php';

session_start();
header('Cache-control: private');

if(isset($_SESSION['access']) && $_SESSION['access']==true) {
    header('Location: /results');
}

if($_POST && isset($_POST['username']) && isset($_POST['password'])) {
    $user = User::getUserByUsername($_POST['username']);
    if($user->username && $user->password==sha1($_POST['password'])) {
        $_SESSION['access'] = true;
        header('Location: /results');
    }
}

?>
<!doctype html>
<html>
    <head>
        <title>Logowanie</title>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <label for="username">Username:</label>
            <input type="text" name="username" id="username">
            <label for="password">Password:</label>
            <input type="password" name="password" id="password">
            <input type="submit" value="Login">
        </form>
        <p id="message"></p>
    </body>
</html>
