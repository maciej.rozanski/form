<?php

include 'lib/DB_Connect.php';
include 'lib/Form.php';

if($_POST) {
    $form = new Form($_POST['firstname'],$_POST['lastname'],$_FILES['image']);
    $form->save();
}
?>
<!doctype html>
<html>
    <head>
        <title>Formularz</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                $('form').submit(function(e){
                    e.preventDefault();
                    var formData = new FormData(this);
                    $.ajax({
                        type:'POST',
                        url: window.location.href,
                        data:formData,
                        dataType:"json",
                        cache:false,
                        contentType: false,
                        processData: false,
                        success:function(response){
                            if(response.status)
                                $('form').trigger("reset");
                            $('#message').text(response.message);
                        },
                        error: function(response){
                            $('#message').text(response.message);
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <label for="firstname">Imię:</label>
            <input type="text" name="firstname" id="firstname" required>
            <label for="firstname">Nazwisko:</label>
            <input type="text" name="lastname" id="lastname">
            <input type="file" name="image" id="image" accept="image/jpeg, image/png">
            <input type="submit" value="Wyślij">
        </form>
        <p id="message"></p>
    </body>
</html>
