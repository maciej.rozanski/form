<?php

session_start();
header('Cache-control: private');

if(!isset($_SESSION['access']) || $_SESSION['access']!=true) {
    header('Location: /login');
}

if($_GET && $_GET['logout']) {
    $_SESSION['access'] = false;
    session_unset();
    session_destroy();
    header('Location: /login');
}

include 'lib/DB_Connect.php';
include 'lib/Form.php';

$records = Form::getRecords();

?>
<!doctype html>
<html>
    <head>
        <title>Lista rekordów</title>
        <style>
            table, th, td {border: 1px solid black;}
            td {padding:10px;}
        </style>
    </head>
    <body>
        <?php if($records->num_rows>0): ?>
            <table>
                <?php while($row = $records->fetch_assoc()): ?>
                    <tr>
                        <td><?= $row['firstname'] ?></td>
                        <td><?= $row['lastname'] ?></td>
                        <td><a target="_blank" href="<?= TARGET_DIR . $row['image'] ?>"><?= $row['image'] ?></a></td>
                    </tr>
                <?php endwhile; ?>
            </table>
        <?php else: ?>
            <p>Brak rekordów</p>
        <?php endif; ?>
        <br><a href="?logout=1">LOGOUT</a>
    </body>
</html>
